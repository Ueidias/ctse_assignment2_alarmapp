/*
 * *
 *  * Created by Ashane Edirisinghe
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 4/10/19 10:55 AM
 *
 */

package com.example.ctse_alarmapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ToneSelectActivity extends AppCompatActivity {

    private RadioGroup radioToneGroup;
    private RadioButton radioToneButton;
    private RadioButton radioToneButton1;
    private RadioButton radioToneButton2;
    private RadioButton radioToneButton3;
    private RadioButton radioToneButton4;
    private RadioButton radioToneButton5;
    MediaPlayer media_tone;
    private SharedPreferences sharedpreference;
    private SharedPreferences.Editor mEditor;
    public String toneSelected= "Alarm5";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tone_select);

        DisplayMetrics dm1 = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm1);

        int width = dm1.widthPixels;
        int height = dm1.heightPixels;

        getWindow().setLayout((int)(width*.65),(int)(height*.45));

        //hide default action bar
        getSupportActionBar().hide();

        radioToneGroup=(RadioGroup)findViewById(R.id.radioGroup);

        radioToneButton1=(RadioButton)findViewById(R.id.radioButton1);
        radioToneButton2=(RadioButton)findViewById(R.id.radioButton2);
        radioToneButton3=(RadioButton)findViewById(R.id.radioButton3);
        radioToneButton4=(RadioButton)findViewById(R.id.radioButton4);
        radioToneButton5=(RadioButton)findViewById(R.id.radioButton5);

        radioToneButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(media_tone != null){
                    media_tone.stop();
                    media_tone.reset();
                }
                media_tone = MediaPlayer.create(ToneSelectActivity.this,R.raw.shape);
                media_tone.start();
                toneSelected = "Alarm1";
            }
        });
        radioToneButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(media_tone != null){
                    media_tone.stop();
                    media_tone.reset();
                }
                media_tone = MediaPlayer.create(ToneSelectActivity.this,R.raw.hello);
                media_tone.start();
                toneSelected = "Alarm2";
            }
        });
        radioToneButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(media_tone != null){
                    media_tone.stop();
                    media_tone.reset();
                }
                media_tone = MediaPlayer.create(ToneSelectActivity.this,R.raw.thinking);
                media_tone.start();
                toneSelected = "Alarm3";
            }
        });
        radioToneButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(media_tone != null){
                    media_tone.stop();
                    media_tone.reset();
                }
                media_tone = MediaPlayer.create(ToneSelectActivity.this,R.raw.bohemian);
                media_tone.start();
                toneSelected = "Alarm4";
            }
        });
        radioToneButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(media_tone != null){
                    media_tone.stop();
                    media_tone.reset();
                }
                media_tone = MediaPlayer.create(ToneSelectActivity.this,R.raw.say);
                media_tone.start();
                toneSelected = "Alarm5";
            }
        });

        Button selctbtn = (Button)findViewById(R.id.slectbtn);
        selctbtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(media_tone != null){
                    media_tone.stop();
                    media_tone.reset();
                }
                int selectedId = radioToneGroup.getCheckedRadioButtonId();
                radioToneButton = (RadioButton)findViewById(selectedId);

                sharedpreference = PreferenceManager.getDefaultSharedPreferences(ToneSelectActivity.this);
                mEditor = sharedpreference.edit();
                mEditor.putString("AlarmToneSelected", toneSelected);
                mEditor.apply();

                String toastmsg = "You selected a tone" + toneSelected;
                Toast.makeText(ToneSelectActivity.this,toastmsg,Toast.LENGTH_SHORT).show();

                Intent intent = new Intent();
                intent.putExtra("tonee", toneSelected);
                setResult(RESULT_OK, intent);

                finish();
            }
        });

        Button cancelbtn =(Button)findViewById(R.id.cncelbtn);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(media_tone != null){
                    media_tone.stop();
                    media_tone.reset();
                }
                finish();
            }
        });
    }
}
