/*
 * *
 *  * Created by Ashane Edirisinghe
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 4/10/19 11:53 AM
 *
 */

package com.example.ctse_alarmapp;

import android.util.Log;

public class Questions {

    public String QuestionSet[] ={
            "Entomology is the science that studies",
            "Galileo was an Italian astronomer who",
            "Exposure to sunlight helps a person improve his health because",
            "The ozone layer restricts",
            "For the Olympics and World Tournaments, the dimensions of basketball court are"
    };
    public String answerList[][]={
            {"Behavior of human beings","Insects","The origin and history of technical and scientific terms","The formation of rocks"},
            {"developed the telescope","discovered four satellites of Jupiter","discovered that the movement of pendulum produces a regular time measurement","All of the above"},
            {"the infrared light kills bacteria in the body","resistance power increases","the pigment cells in the skin get stimulated and produce a healthy tan","the ultraviolet rays convert skin oil into Vitamin D"},
            {"Visible light","Infrared radiation","X-rays and gamma rays","Ultraviolet radiation"},
            {"26 m x 14 m","28 m x 15 m","27 m x 16 m","28 m x 16 m"},
    };
    public String correctAnswer[]={
            "Insects",
            "All of the above",
            "the ultraviolet rays convert skin oil into Vitamin D",
            "Ultraviolet radiation",
            "28 m x 15 m"
    };
//   reference: https://www.indiabix.com/general-knowledge/basic-general-knowledge/005008

    public String getQuestion(int num){
        return QuestionSet[num];
    };

    public String getChoice1(int num){
        return answerList[num][0];
    };

    public String getChoice2(int num){
        return answerList[num][1];
    };

    public String getChoice3(int num){
        return answerList[num][2];
    };

    public String getChoice4(int num){
        return answerList[num][3];
    };

    public String getCorrectAnswer(int num){
            return correctAnswer[num];
    };

}
