/*
 * *
 *  * Created by Ashane Edirisinghe
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 4/9/19 8:55 PM
 *
 */

package com.example.ctse_alarmapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private final static int REQUEST_CODE_1 = 1;

    private SharedPreferences sharedpreference;
    private SharedPreferences.Editor mEditor;

    Button alrmbtn;
    TextView alrmName,alrmTime;
    Boolean intent_recieved = false;
    ImageView alrmImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();
        alrmName =(TextView)findViewById(R.id.alrmName);
        alrmTime =(TextView)findViewById(R.id.alrmTime);
        alrmImage = (ImageView) findViewById(R.id.alrmImage);

        sharedpreference = PreferenceManager.getDefaultSharedPreferences(this);
        String sName = sharedpreference.getString("AlarmName","");
        String sTime = sharedpreference.getString("AlarmTime","");
        alrmTime.setText(sTime);
        alrmName.setText(sName);

        if(sTime.equals("")){
            alrmImage.setVisibility(View.INVISIBLE);
        }else{
            alrmImage.setVisibility(View.VISIBLE);
        }

        alrmbtn = (Button) findViewById(R.id.alrmbtn);
        alrmbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                intent_recieved = true;
                Intent i = new Intent(getBaseContext(), AlarmActivity.class);
                i.putExtra("Source","MainActivity");
                startActivityForResult(i, REQUEST_CODE_1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // The returned result data is identified by requestCode.
        // The request code is specified in startActivityForResult(intent, REQUEST_CODE_1); method.
        switch (requestCode)
        {
            // This request code is set by startActivityForResult(intent, REQUEST_CODE_1) method.
            case REQUEST_CODE_1:
                if(resultCode == RESULT_OK)
                {
                    String messageReturn = data.getStringExtra("time");
                    String messageReturn1 = data.getStringExtra("name");
                    alrmTime.setText(messageReturn);
                    alrmName.setText(messageReturn1);

                    if(messageReturn.equals("")) {
                        alrmImage.setVisibility(View.INVISIBLE);
                    }else{
                        //inserting alarm time and date to shared preference
                        sharedpreference = PreferenceManager.getDefaultSharedPreferences(this);
                        mEditor = sharedpreference.edit();
                        mEditor.putString("AlarmName", messageReturn1);
                        mEditor.putString("AlarmTime", messageReturn);
                        mEditor.commit();
                        alrmImage.setVisibility(View.VISIBLE);
                    }
                }
        }
    }
}
