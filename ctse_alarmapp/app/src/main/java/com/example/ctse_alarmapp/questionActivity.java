/*
 * *
 *  * Created by Ashane Edirisinghe
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 4/9/19 3:08 PM
 *
 */

package com.example.ctse_alarmapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class questionActivity extends AppCompatActivity {

    private RequestQueue mQueue;

    TextView Question;
    private RadioGroup radioAnsGroup;
    private RadioButton radioAnsButton;
    private RadioButton radioAnsButton1;
    private RadioButton radioAnsButton2;
    private RadioButton radioAnsButton3;
    private RadioButton radioAnsButton4;
    String Answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        //hide default action bar
        getSupportActionBar().hide();
        radioAnsGroup=(RadioGroup)findViewById(R.id.radioGroup);

        Question =(TextView)findViewById(R.id.QuestionText);
        radioAnsButton1=(RadioButton)findViewById(R.id.radioButton1);
        radioAnsButton2=(RadioButton)findViewById(R.id.radioButton2);
        radioAnsButton3=(RadioButton)findViewById(R.id.radioButton3);
        radioAnsButton4=(RadioButton)findViewById(R.id.radioButton4);

        try{
            this.retriveQuestions();
            if(Question.getText().equals("QuestionText")){
                //triggered in occasions like when no internet in app
                this.createNewQuestion();
            }
        }catch(Exception e){
            Log.e("ERROR IN Questions",e.toString());
            this.createNewQuestion();
        }

        Button questionbtn = (Button)findViewById(R.id.questionbtn);
        questionbtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int selectedId = radioAnsGroup.getCheckedRadioButtonId();
                if(selectedId > 0){
                radioAnsButton = (RadioButton)findViewById(selectedId);
                if(radioAnsButton.getText().equals(Answer)) {
                    String toastmsg = "Correct answer , Alarm will stop now";
                    Toast.makeText(questionActivity.this,toastmsg,Toast.LENGTH_SHORT).show();
                    Intent intent;
                    intent = new Intent(getBaseContext(), AlarmActivity.class);
                    intent.putExtra("textchange", "Alarm Off");
                    intent.putExtra("Source", "questionActivity");
                    startActivity(intent);
                    finish();
                }else{
                    radioAnsButton.setChecked(false);
                    String toastmsg = "Incorrect answer , Try again";
                    Toast.makeText(questionActivity.this,toastmsg,Toast.LENGTH_SHORT).show();

                    createNewQuestion();
                }}
            }
        });
    }

    // In-class question collection
    public void createNewQuestion(){
        Log.e("QUESTION SCENARIO","STARTED");
        Questions ques = new Questions();
        final int min = 0;
        final int max = (ques.QuestionSet.length) - 1;
        final int random = new Random().nextInt((max - min) + 1) + min;

        String Q = ques.getQuestion(random);
        String A = ques.getChoice1(random);
        String B = ques.getChoice2(random);
        String C = ques.getChoice3(random);
        String D = ques.getChoice4(random);
        Answer = ques.getCorrectAnswer(random);

        Log.e("QUESTION SCENARIO",""+Q+":"+"1."+A+"2."+B+"3."+C+"4."+D+"->"+Answer);
        Question.setText(Q);
        radioAnsButton1.setText(A);
        radioAnsButton2.setText(B);
        radioAnsButton3.setText(C);
        radioAnsButton4.setText(D);
    }

    //retrieve the question from an external api
    public void retriveQuestions(){
        mQueue= Volley.newRequestQueue(this);
        this.jsonParse();
        Log.e("QUESTION SERVER","Initialized");
    }

    //getting questions using volley library
    private void jsonParse() {
        // change this url to retrieve questions from new url
        String url="https://gist.githubusercontent.com/AshaneEdiri/f796f7782d002c43aaaa5c5a99d5e279/raw/98cfd8f18a0705c6a8b001dc51eab0ab9b736477/gkQuestions";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("gkquestions");

                            int max1=jsonArray.length()-1;
                            int min1 = 0;
                            final int random1 = new Random().nextInt((max1 - min1) + 1) + min1;
                            JSONObject gkquestions= jsonArray.getJSONObject(random1);
                            String ques = gkquestions.getString("question");

                            JSONObject Anss = gkquestions.getJSONArray("answerlist").getJSONObject(0);
                            String Ans1 = Anss.getString("ans1");
                            String Ans2 = Anss.getString("ans2");
                            String Ans3 = Anss.getString("ans3");
                            String Ans4 = Anss.getString("ans4");
                            String Ans = gkquestions.getString("answer");

                            Answer = Ans;

                            Question.setText(ques);
                            radioAnsButton1.setText(Ans1);
                            radioAnsButton2.setText(Ans2);
                            radioAnsButton3.setText(Ans3);
                            radioAnsButton4.setText(Ans4);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }
}
