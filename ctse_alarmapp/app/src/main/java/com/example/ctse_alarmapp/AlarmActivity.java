/*
 * *
 *  * Created by Ashane Edirisinghe
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 4/10/19 10:44 AM
 *
 */

package com.example.ctse_alarmapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class AlarmActivity extends AppCompatActivity {

    private final static int REQUEST_CODE_2 = 2;
    private SharedPreferences sharedpreference;
    private SharedPreferences.Editor mEditor;
    AlarmManager alarm_manager;
    TimePicker alarm_timepicker;
    TextView alarmName,alarmTone,alarmnameedit,alarmtoneedit;
    TextView statusText;
    Context context;
    PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.80),(int)(height*.80));

        //hide default action bar
        getSupportActionBar().hide();

        //convert time picker widget into 24h format
        TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        this.context = this;

        //alarm manager initialization
        alarm_manager=(AlarmManager) getSystemService(ALARM_SERVICE);
        //timepicker initialization
        alarm_timepicker=(TimePicker)findViewById(R.id.timePicker);

        alarmName =(TextView)findViewById(R.id.alarmname);
        alarmnameedit = (TextView)findViewById(R.id.alarmname_edit);
        alarmTone =(TextView)findViewById(R.id.tone);
        alarmtoneedit=(TextView)findViewById(R.id.alarmtone_edit);
        statusText=(TextView)findViewById(R.id.statusText);

        //Use of shared preference
        sharedpreference = PreferenceManager.getDefaultSharedPreferences(this);
        String sName = sharedpreference.getString("AlarmName","");
        String sTone = sharedpreference.getString("AlarmToneSelected","tone3");
        int sHour = sharedpreference.getInt("AlarmHour",-1);
        int sMinute = sharedpreference.getInt("AlarmMinute",-1);
        if(sHour != -1) {
            alarm_timepicker.setHour(sHour);
            alarm_timepicker.setMinute(sMinute);
            alarmnameedit.setText(sName);
            alarmtoneedit.setText(sTone);
        }

        final Calendar calender = Calendar.getInstance();
        final Intent my_intent = new Intent(this.context,Alarm_Receiver.class);

        Button alarm_on = (Button)findViewById(R.id.savebtn);
        alarm_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calender.set(Calendar.HOUR_OF_DAY,alarm_timepicker.getHour());
                calender.set(Calendar.MINUTE,alarm_timepicker.getMinute());
                calender.set(Calendar.SECOND, 00);

                int hour = alarm_timepicker.getHour();
                int minute = alarm_timepicker.getMinute();
                String almName = alarmnameedit.getText().toString();

                String hour_string =String.valueOf(hour);
                String minute_string =String.valueOf(minute);

                //put values to shared preference
                mEditor = sharedpreference.edit();
                mEditor.putInt("AlarmHour", hour);
                mEditor.putInt("AlarmMinute", minute);
                mEditor.apply();
//                //To display in 12 hour format
//                if(hour > 12){
//                    hour_string =String.valueOf(hour-12);
//                }
                if((hour < 10)){
                    hour_string = "0"+ hour_string;
                }
                if(minute < 10){
                    minute_string = "0"+String.valueOf(minute);
                }

                set_alarm_text("Alarm set to " + hour_string +" : "+minute_string);

                //put in extra string into intent
                my_intent.putExtra("extra","alarm on");

                //create a pending intent--> delay intent
                pendingIntent = PendingIntent.getBroadcast(AlarmActivity.this,0,my_intent,PendingIntent.FLAG_UPDATE_CURRENT);

                //set the alarm manager
                alarm_manager.set(AlarmManager.RTC_WAKEUP,calender.getTimeInMillis(),pendingIntent);

                Intent intent = new Intent();
                intent.putExtra("time", hour_string +" : "+minute_string);
                intent.putExtra("name", almName);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        Button alarm_off =(Button)findViewById(R.id.cancelbtn);
        alarm_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor = sharedpreference.edit();
                mEditor.remove("AlarmName");
                mEditor.remove("AlarmHour");
                mEditor.remove("AlarmMinute");
                mEditor.remove("AlarmToneSelected");

                mEditor.apply();

                set_alarm_text("Alarm off!");

                pendingIntent = PendingIntent.getBroadcast(AlarmActivity.this,0,my_intent,PendingIntent.FLAG_UPDATE_CURRENT);
                alarm_manager.cancel(pendingIntent);
                my_intent.putExtra("extra","alarm off");

                //stop ringing tone
                sendBroadcast(my_intent);

                Intent intent = new Intent();
                intent.putExtra("time", "");
                intent.putExtra("name", "");
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        //Get the intent from the quiz service
        Intent intentExtras = getIntent();
        String source = intentExtras.getStringExtra("Source");
        if(source.equals("questionActivity")){
            String messageReturn = intentExtras.getStringExtra("textchange");
            Log.e("Intent from quiz",messageReturn);
            pendingIntent = PendingIntent.getBroadcast(AlarmActivity.this,0,my_intent,PendingIntent.FLAG_UPDATE_CURRENT);
            try{
                alarm_manager.cancel(pendingIntent);
                my_intent.putExtra("extra","alarm off");
                //stop ringing tone
                sendBroadcast(my_intent);
                Intent intent;
                intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
                //finish();
            }catch (Exception e){
                my_intent.putExtra("extra","alarm off");
                finish();
            }
        }
    }

    private void set_alarm_text(String output) {
        statusText.setText(output);
    }

    public void alarmClicked(View view){
        Intent in = new Intent(getBaseContext(), ToneSelectActivity.class);
        in.putExtra("Source","AlarmActivity");
        startActivityForResult(in, REQUEST_CODE_2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // The returned result data is identified by requestCode.
        switch (requestCode)
        {
            case REQUEST_CODE_2:
                if(resultCode == RESULT_OK)
                {
                    String messageReturn = data.getStringExtra("tonee");
                    alarmtoneedit.setText(messageReturn);
                }
        }
    }
}
