/*
 * *
 *  * Created by Ashane Edirisinghe
 *  * Copyright (c) 2019 . All rights reserved.
 *  * Last modified 4/10/19 10:55 AM
 *
 */

package com.example.ctse_alarmapp;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.security.Provider;

public class RingingtonePlayingService extends Service {

    MediaPlayer media_song;
    int startId;
    boolean isRunning = false;
    String state = "alarm off";
    private SharedPreferences sharedpreference;
    private SharedPreferences.Editor mEditor;
    String ToneChoice;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent,int flags,int startId){

        Log.e("LocalService","Received Start Id" +startId+":"+intent);

        //fetch the extra string vals
        state = intent.getExtras().getString("extra");

        Log.e("Ringtone state:extra is",state);

        //convert extra string from intent
        assert  state != null;
        switch (state) {
            case "alarm on":
                startId = 1;
                break;
            case "alarm off":
                startId = 0;
                Log.e("Start ID is",state);
                break;
            default:
                startId = 0;
                break;
        }

        //if else statements
        //no music,user pressed on
        if((!this.isRunning) && (startId == 1)){
            Log.e("there is no music","and you want start");

            sharedpreference = PreferenceManager.getDefaultSharedPreferences(this);
            ToneChoice = sharedpreference.getString("AlarmToneSelected","");

            if(ToneChoice.equals("Alarm1")){
                media_song = MediaPlayer.create(this,R.raw.shape);
            }else if(ToneChoice.equals("Alarm2")){
                media_song = MediaPlayer.create(this,R.raw.hello);
            }else if(ToneChoice.equals("Alarm3")){
                media_song = MediaPlayer.create(this,R.raw.thinking);
            }else if(ToneChoice.equals("Alarm4")){
                media_song = MediaPlayer.create(this,R.raw.bohemian);
            }else{
                media_song = MediaPlayer.create(this,R.raw.say);
            }
            media_song.start();

            this.isRunning = true;
            this.startId = 0;

            // notification
            NotificationManager notify_Manager = (NotificationManager)
                    getSystemService(NOTIFICATION_SERVICE);
            //set up an intent to go to main activity
            Intent intent_main_activity = new Intent(this.getApplicationContext(),questionActivity.class);

            PendingIntent pending_intent_main_activity = PendingIntent.getActivity(this,0,
                    intent_main_activity,0);

            Notification notification_popup = new Notification.Builder(this)
                    .setContentTitle("Alarm is active")
                    .setContentText("Click here to stop")
                    .setContentIntent(pending_intent_main_activity)
                    .setSmallIcon(R.drawable.ic_alarm_imgnew)
                    .setAutoCancel(true)
                    .build();

            //set up nevigation call method
            assert notify_Manager != null;
            notify_Manager.notify(0,notification_popup);

            //To Android versions above 26+, access notification is different
            //in such cases question activity will directly start
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startActivity(intent_main_activity);
            }
        }
        //if music playing,user pressed off
        else if(this.isRunning && startId == 0){
            Log.e("there is music","and you want end");
            media_song.stop();
            media_song.reset();

            this.isRunning = false;
            this.startId = 0;
        }
        //if user presses random buttons
        else if((!this.isRunning) && (startId == 0)){
            Log.e("there is no music","and you want end");

            this.isRunning = false;
            this.startId = 0;
        }
        //if music plays and user pressed on
        else if(this.isRunning && startId == 1){
            Log.e("there is music","and you want start");
            this.isRunning = true;
            this.startId = 1;
        }
        else{
            Log.e("else","you somehow reached");

        }
        return START_NOT_STICKY;
    };

    public void onDestroy(){
//        Toast.makeText(this,"on Destroy called",Toast.LENGTH_SHORT).show();
        Log.e("on destroy called",":)");
        super.onDestroy();
        this.isRunning = false;
    }
}
